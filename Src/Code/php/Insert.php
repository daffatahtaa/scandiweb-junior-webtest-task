<?php

abstract class AttributeValue
{
  abstract function value();
  abstract function setValue($attr);
  abstract function getValue();
}

class Dvd extends AttributeValue
{
  public $size;
  public function setValue($size)
  {
    $this->size = $size;
  }

  public function getValue()
  {
    return $this->size;
  }

  public function value()
  {
    $size = !empty($_POST["size"]) ? $_POST["size"] : "null";
    $this->setValue($size);
    return $this->getValue()  . "Mb";
  }
}

class Books extends AttributeValue
{
  public $weight;
  public function setValue($attr)
  {
    $this->weight = $attr;
  }

  public function getValue()
  {
    return $this->weight;
  }

  public function value()
  {
    $weight = !empty($_POST["weight"]) ? $_POST["weight"] : "null";
    $this->setValue($weight);
    return $this->getValue() . "Kg";
  }
}

class Furniture
{
  public $height;
  public $width;
  public $length;
  public function setValue($attr, $attr2, $attr3)
  {
    $this->height = $attr;
    $this->width = $attr2;
    $this->length = $attr3;
  }

  public function getValue()
  {
    return $this->height . "X" . $this->width . "X" . $this->length;
  }

  public function value()
  {
    $height = !empty($_POST["height"]) ? $_POST["height"] : "null";
    $width = !empty($_POST["width"]) ? $_POST["width"] : "null";
    $length = !empty($_POST["length"]) ? $_POST["length"] : "null";
    $this->setValue($height, $width, $length);
    return $this->getValue();
  }
}

class Data
{
  public $sku;
  public $name;
  public $price;
  public $attribute;
  public $product_type;

  public function setProductType($product_type)
  {
    $this->product_type = $product_type;
  }

  public function setSku($sku)
  {
    $this->sku = $sku;
  }
  public function setName($name)
  {
    $this->name = $name;
  }
  public function setPrice($price)
  {
    $this->price = $price;
  }
  public function setAttribute($attribute)
  {
    $this->attribute = $attribute;
  }

  public function getSku()
  {
    return $this->sku;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getPrice()
  {
    return $this->price;
  }

  public function getAttribute()
  {
    return $this->attribute;
  }

  public function getProductType()
  {
    return $this->product_type;
  }

  public function SetProductData()
  {
    $this->setSku($_POST["SKU"]);
    $this->setName($_POST["name"]);
    $this->setPrice($_POST["price"]);
    $this->setProductType($_POST["productType"]);
  }
}

class insert
{
  public function insert_into_db()
  {
    $sqli = new mysqli("localhost", "root", "", "main");
    // $sqli = new mysqli("localhost", "id17775516_admin", "r[!ihe%]VT8|d>d_", "id17775516_scandiwebdb");

    $data = new Data();
    $dvd = new Dvd();
    $books = new Books();
    $furniture = new Furniture();

    !empty($_POST["SKU"]) ? $data->SetProductData() : "Sum ting wong";

    $sku = $data->getSku();
    $name = $data->getName();
    $price = $data->getPrice();
    $productType = $data->getProductType();

    $attribute = array(
      1 => $dvd->value(),
      2 => $books->value(),
      3 => $furniture->value()
    );

    ($productType == "DVD") ? $sqli->query("INSERT INTO `product` (`id`, `SKU`, `Name`, `Price`, `Product-attribute`) VALUES (NULL, '$sku', '$name', '$price', '$attribute[1]')") : (($productType == "Book") ? $sqli->query("INSERT INTO `product` (`id`, `SKU`, `Name`, `Price`, `Product-attribute`) VALUES (NULL, '$sku', '$name', '$price', '$attribute[2]')") : (($productType == "Furniture") ? $sqli->query("INSERT INTO `product` (`id`, `SKU`, `Name`, `Price`, `Product-attribute`) VALUES (NULL, '$sku', '$name', '$price', '$attribute[3]')") : ""));
  }
}
