<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Add Product Page</title>
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Ruda&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="/Src/Code/css/style.css">

</head>

<body>
  <header>
    <a href="">
      <h4>Product Add</h4>
    </a>
    <nav>
      <ul class="nav-links">
        <li>
          <button id="save-btn" type="submit" form="productForm" onclick="saveOnClick()">Save</button>
        </li>
        <li>
          <a href="/"><button id="cancel-btn">Cancel</button></a>
        </li>
      </ul>
    </nav>
  </header>
  <hr>
  <form action="" id="productForm" method="POST">
    <div class="container-fluid">
      <div class="row">
        <table class="table table-borderless">
          <tbody>
            <tr>
              <td>
                <label for="SKU">SKU</label>
              </td>
              <td>
                <input type="text" name="SKU" id="sku" class="form-data" required />
              </td>
            </tr>
            <tr>
              <td>
                <label for="NAME">Name</label>
              </td>
              <td>
                <input type="text" name="name" id="name" class="form-data" required />
              </td>
            </tr>
            <tr>
              <td>
                <label for="Price">Price ($)</label>
              </td>
              <td>
                <input type="number" name="price" id="price" class="form-data" required />
              </td>
            </tr>
            <tr>
              <td>
                <label for="Combo-Box">Type</label>
              </td>
              <td>
                <select name="productType" id="productType" onchange="comboBox()">
                  <option value="DVD">DVD</option>
                  <option value="Book">Book</option>
                  <option value="Furniture">Furniture</option>
                </select>
              </td>
            </tr>
            <tr id="group-1">
              <td>
                <label for="size">Size</label>
              </td>
              <td>
                <input type="text" name="size" id="size" class="form-data">
              </td>
            </tr>
            <tr id="group-2">
              <td>
                <label for="width">Weight</label>
              </td>
              <td>
                <input type="text" name="weight" id="weight" class="form-data">
              </td>
            </tr>
            <tr id="group-3">
              <td>
                <label for="length" id="label-input-3">Height: </label><br>
                <label for="length" id="label-input-3">Width: </label><br>
                <label for="length" id="label-input-3">Length: </label>
              </td>
              <td>
                <input type="text" name="height" id="height" class="form-data"><br>
                <input type="text" name="width" id="width" class="form-data"><br>
                <input type="text" name="length" id="length" class="form-data">
              </td>
            </tr>
          </tbody>
        </table>
        <div id="desc">
          <p id="desc-value">
            null
          </p>
        </div>
      </div>
    </div>
  </form>
  <script>
    document.getElementById("productType").selectedIndex = -1;
    document.getElementById("desc").style.visibility = "hidden";

    var group1 = document.getElementById("group-1");
    var group2 = document.getElementById("group-2");
    var group3 = document.getElementById("group-3");

    group1.style.visibility = "hidden";
    group2.style.visibility = "hidden";
    group3.style.visibility = "hidden";

    function comboBox() {
      var selected = document.getElementById("productType");
      var valueSelected = selected.options[selected.selectedIndex].value;

      var descriptions = document.getElementById("descriptions");

      (valueSelected == "DVD") ? dvd(): ((valueSelected == "Book") ? books() : ((valueSelected == "Furniture") ? furniture() : ""));
      console.log(valueSelected);
    }

    function dvd() {
      group1.style.visibility = "visible";
      group2.style.visibility = "hidden";
      group3.style.visibility = "hidden";
      document.getElementById("size").required = true;
      document.getElementById("desc").style.visibility = "visible";
      document.getElementById("desc-value").innerHTML = "Please, provide size.";
      document.getElementById("desc").style.visibility = "visible";
      document.getElementById("desc-value").innerHTML = "Please, provide size.";
    }

    function books() {
      group1.style.visibility = "hidden";
      group2.style.visibility = "visible";
      group3.style.visibility = "hidden";
      document.getElementById("desc").style.visibility = "visible";
      document.getElementById("desc-value").innerHTML = "Please, provide weight.";
    }

    function furniture() {
      group1.style.visibility = "hidden";
      group2.style.visibility = "hidden";
      group3.style.visibility = "visible";
      document.getElementById("desc").style.visibility = "visible";
      document.getElementById("desc-value").innerHTML = "Please, provide dimensions.";
    }

    function saveOnClick() {
      console.log("Clicked btn");
    }
  </script>
</body>
<?php
include "./Src/Code/php/Insert.php";
$insert = new insert();
$insert->insert_into_db();
?>

</html>